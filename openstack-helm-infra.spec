Name: openstack-helm-infra
Version: ff70971009d29a37619e8e82080663a9ab76d57b
Release: 0
Summary: Charts for services or integration of third-party solutions that are required to run OpenStack-Helm

License: ASL 2.0
URL: https://opendev.org/openstack/openstack-helm-infra
Source0: openstack-helm-infra-%{version}.tar.gz

BuildArch: noarch

Requires: ca-certificates
Requires: git
Requires: make
Requires: jq
Requires: nmap
Requires: curl
Requires: python3-pip
Requires: bc
Requires: util-linux

%description
Charts for services or integration of third-party solutions that are required to run OpenStack-Helm

%prep
%setup -q -n openstack-helm-infra-%{version}

%build

%install
install -d -m755 %{buildroot}%{_datadir}/openstack-helm
cp -vr %{_builddir}/openstack-helm-infra-%{version} %{buildroot}%{_datadir}/openstack-helm/openstack-helm-infra

%files
%{_datadir}/openstack-helm/openstack-helm-infra

%changelog
* Tue Aug 30 2022 Yinuo Deng <i@dyn.im> - ff70971009d29a37619e8e82080663a9ab76d57b-0
- Initial version